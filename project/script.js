//1.Опишіть своїми словами що таке Document Object Model (DOM)
//DOM має деревоподібну ієархію. Документ DOM складається з вузлів Node. Кожен вузол може містити у собі вбудований вузол, елемент, текст чи коментар.

//2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//innerText -властивість, яка виводить лише текст проміж откриваючим та закриваючим тегом елементом, а innerHTML - буде виводити не тільки текст а ще допоміжні тегі (наприклад якщо текст виведен жирним). 

//3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// - За ідентифікатором (id)- document.getElementById('');
// - За класом - document.getElementsByClassName('');
// - За тегом - document.getElementsByTagName('');
// - За CSS селектором (один елемент) - document.querySelector('');
// - За CSS селектором (багато елементів) - document.querySelectorAll('').
// Усі методи "getElementsBy*" повертають живу колекцію. Такі колекції завжди відображають поточний стан документа і “автооновлюються” при його зміні.На відміну від цього, querySelectorAll повертає статичну колекцію. Це схоже на фіксований масив елементів.

//Завдання: 
// 1.Знайти всі параграфи на сторінці та встановити колір фону #ff0000
    
    let pElements = document.getElementsByTagName('p');
    console.log(pElements);   
    for (pElem of pElements) {
        pElem.style.background = '#ff0000';
    };

// 2.Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
    
    console.log(document.getElementById('optionsList'));
    console.log(document.getElementById('optionsList').parentElement);
    Array.from(document.getElementById('optionsList').childNodes).forEach(node => {
        console.log(node.nodeName);
        console.log(node.nodeType);
    })

// 3.Встановіть в якості контента елемента з класом testParagraph наступний параграф -This is a paragraph

    document.getElementById('testParagraph').textContent = 'This is a paragraph';
    
// 4.Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
    
    let elements = document.querySelector('.main-header').children;
    console.log(elements);
    for (let element of elements) {
        console.log(element);
        element.classList.add("nav-item")
    };
    console.log(elements);
 
// 5.Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

    let elem = document.querySelectorAll('.section-title');
    console.log(elem);
    for (let el of elem) {
        el.classList.remove('section-title');
    }
    console.log(elem);
